import { Sequelize } from 'sequelize';

class SequelizeUtil {
    static instance;

    static init() {
        this.instance = new Sequelize('expensetracker', process.env.DB_USER, process.env.DB_PASS, {
            host: process.env.HOST,
            dialect: 'postgres'
        });
    } 
}

export default SequelizeUtil;