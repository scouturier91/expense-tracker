import express, { Application } from 'express';
import IController from './controllers/icontroller';

class App {
    public app: Application;
    public port: number;

    constructor(initData: {port: number; middleWares: any; controllers: any;}) {
        this.app = express();
        this.port = initData.port;
        this.initMiddleWares(initData.middleWares);
        this.initRoutes(initData.controllers);

        this.loadAssets();
    }

    /**
     * Add the passed in middlewares to the application.
     * @param middleWares the middleWares to add to the application.
     */
    private initMiddleWares(middleWares: Array<any>) {
        middleWares.forEach(mw => this.app.use(mw));
    }

    /**
     * Initialize the routes for the application.
     * @param controllers The route controllers.
     */
    private initRoutes(controllers: Array<any>) {
        controllers.forEach((controller: IController) => {
            this.app.use('/', controller.router);
        });
    }

    /**
     * Load any static assets here from the assets folder.
     */
    private loadAssets() {

    }

    /**
     * Start the application listening on the configured port.
     */
    public listen() {
        this.app.listen(this.port, () => 
            console.log(`App is runnning at http://localhost:${this.port}`));
    }
}

export default App;