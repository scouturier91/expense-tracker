import express from 'express';

interface IController {
    path: string;
    router: express.Router;
    initRoutes(): any;
}

export default IController;