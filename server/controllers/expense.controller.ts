import { Request, Response } from 'express';
import * as express from 'express';
import Expense from '../models/expense';
import IController from './icontroller';

class ExpenseController implements IController {
    public path = '/expense';
    public router = express.Router();

    constructor() {
        this.initRoutes();
    }

    public initRoutes() {
        this.router.get(this.path, this.getAll);
    }

    private getAll = async (req: Request, res: Response) => {
        res.json(await Expense.findAll());
    }
}

export default ExpenseController;