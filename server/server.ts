import dotenv from 'dotenv';
dotenv.config({path: './.env'});

import App from './app';

// Middlewares
import * as bodyParser from 'body-parser';

// Controllers
import ExpenseController from './controllers/expense.controller';
import UserController from './controllers/user.controller';

import SequelizeUtil from './sequelize-util';
// Initialize the sequelize instance
SequelizeUtil.init();

const app = new App({
    port: +process.env.PORT,
    controllers: [
        new ExpenseController(),
        new UserController()
    ],
    middleWares: [
        bodyParser.json()
    ]
});

// Start the application
app.listen();