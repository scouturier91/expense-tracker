import { Optional, DataTypes, Model } from "sequelize";
import SequelizeUtil from '../sequelize-util';

interface ExpenseAttributes {
    id: number;
    userid: number;
    createdat: Date;
    amount: number;
}

interface ExpenseCreationAttributes extends Optional<ExpenseAttributes, "id"> {};

class Expense extends Model<ExpenseAttributes, ExpenseCreationAttributes> implements ExpenseAttributes {
    public id: number;
    public userid: number;
    public createdat: Date;
    public amount: number;
    
}

const sequelize = SequelizeUtil.instance;

Expense.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    userid: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    createdat: {
        type: DataTypes.DATE,
        allowNull: false
    },
    amount: {
        type: DataTypes.NUMBER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'expense',
    timestamps: false
});

export default Expense;