import { Optional, DataTypes, Model } from "sequelize";
import SequelizeUtil from "../sequelize-util";

interface UserAttributes {
    id: number;
    username: string;
    email: string;
    pass: string;
    createdat: Date;
}

interface UserCreationAttributes extends Optional<UserAttributes, "id"> {}

class User extends Model<UserAttributes, UserCreationAttributes> implements UserAttributes {
    public id: number;
    public username: string;
    public email: string;
    public pass: string;
    public createdat: Date;
}

const sequelize = SequelizeUtil.instance;

User.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    pass: {
        type: DataTypes.STRING,
        allowNull: false
    },
    createdat: {
        type: DataTypes.DATE,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'user',
    timestamps: false
});

export default User;