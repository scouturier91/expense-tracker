CREATE TABLE IF NOT EXISTS expense (
    id  SERIAL,
    userid  INTEGER,
    createdAt   DATE,
    amount  NUMERIC(2)
);