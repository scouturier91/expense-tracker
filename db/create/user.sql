CREATE TABLE IF NOT EXISTS user (
    id SERIAL,
    username VARCHAR(30),
    email VARCHAR(75),
    pass VARCHAR(100),
    createdAt DATE
);