declare global {
    namespace NodeJS {
        interface ProcessEnv {
            PORT: string,
            ENV: string,
            HOST: string,
            DB_USER: string,
            DB_PASS: string
        }
    }
}

export {};